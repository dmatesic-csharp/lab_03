﻿namespace PomodoroApp
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblRad = new System.Windows.Forms.Label();
            this.lblOdmor = new System.Windows.Forms.Label();
            this.lblVrijeme = new System.Windows.Forms.Label();
            this.tbOdmor = new System.Windows.Forms.TextBox();
            this.tbRad = new System.Windows.Forms.TextBox();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.tmrVrijeme = new System.Windows.Forms.Timer(this.components);
            this.lblStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblRad
            // 
            this.lblRad.AutoSize = true;
            this.lblRad.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblRad.Location = new System.Drawing.Point(61, 51);
            this.lblRad.Name = "lblRad";
            this.lblRad.Size = new System.Drawing.Size(87, 38);
            this.lblRad.TabIndex = 0;
            this.lblRad.Text = "RAD";
            // 
            // lblOdmor
            // 
            this.lblOdmor.AutoSize = true;
            this.lblOdmor.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblOdmor.Location = new System.Drawing.Point(592, 51);
            this.lblOdmor.Name = "lblOdmor";
            this.lblOdmor.Size = new System.Drawing.Size(144, 38);
            this.lblOdmor.TabIndex = 1;
            this.lblOdmor.Text = "ODMOR";
            // 
            // lblVrijeme
            // 
            this.lblVrijeme.AutoSize = true;
            this.lblVrijeme.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblVrijeme.Location = new System.Drawing.Point(318, 201);
            this.lblVrijeme.Name = "lblVrijeme";
            this.lblVrijeme.Size = new System.Drawing.Size(0, 51);
            this.lblVrijeme.TabIndex = 2;
            // 
            // tbOdmor
            // 
            this.tbOdmor.Location = new System.Drawing.Point(518, 116);
            this.tbOdmor.Name = "tbOdmor";
            this.tbOdmor.PlaceholderText = "5";
            this.tbOdmor.Size = new System.Drawing.Size(221, 27);
            this.tbOdmor.TabIndex = 3;
            this.tbOdmor.TabStop = false;
            this.tbOdmor.Text = "5";
            // 
            // tbRad
            // 
            this.tbRad.Location = new System.Drawing.Point(61, 116);
            this.tbRad.Name = "tbRad";
            this.tbRad.PlaceholderText = "25";
            this.tbRad.Size = new System.Drawing.Size(196, 27);
            this.tbRad.TabIndex = 4;
            this.tbRad.Text = "25";
            // 
            // btnStartStop
            // 
            this.btnStartStop.Location = new System.Drawing.Point(61, 312);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(196, 77);
            this.btnStartStop.TabIndex = 5;
            this.btnStartStop.Text = "Start/Stop";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(518, 312);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(221, 77);
            this.btnReset.TabIndex = 6;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // tmrVrijeme
            // 
            this.tmrVrijeme.Interval = 10;
            this.tmrVrijeme.Tick += new System.EventHandler(this.tmrVrijeme_Tick);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblStatus.Location = new System.Drawing.Point(279, 39);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 38);
            this.lblStatus.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnStartStop);
            this.Controls.Add(this.tbRad);
            this.Controls.Add(this.tbOdmor);
            this.Controls.Add(this.lblVrijeme);
            this.Controls.Add(this.lblOdmor);
            this.Controls.Add(this.lblRad);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label lblRad;
        private Label lblOdmor;
        private Label lblVrijeme;
        private TextBox tbOdmor;
        private TextBox tbRad;
        private Button btnStartStop;
        private Button btnReset;
        private System.Windows.Forms.Timer tmrVrijeme;
        private Label lblStatus;
    }
}