namespace PomodoroApp {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
            pomodoro = new Pomodoro(
                true,
                int.Parse(tbRad.Text) * 60,
                int.Parse(tbOdmor.Text) * 60);
        }

        public void RefreshClock() {
            lblVrijeme.Text = pomodoro.ToString();
        }

        public void RefreshStatus() {
            lblStatus.Text = pomodoro.Status();
        }
        private void btnStartStop_Click(object sender, EventArgs e) {
            tmrVrijeme.Enabled = !tmrVrijeme.Enabled;
           if (pomodoro.WorkInProgress) {
                tbRad.Enabled = false;
                tbOdmor.Enabled = false;
            }
           RefreshStatus();
        }

        private void tmrVrijeme_Tick(object sender, EventArgs e) {
            pomodoro.Duration();
            RefreshStatus();
            RefreshClock();
        }
           

        private void btnReset_Click(object sender, EventArgs e) {
            tmrVrijeme.Stop();
            tmrVrijeme.Enabled = false;
            try {
                pomodoro = new Pomodoro(
               true,
               int.Parse(tbRad.Text) * 60,
               int.Parse(tbOdmor.Text) * 60);
            }
            catch (Exception){
                MessageBox.Show("POGRESKA!");
            }
            lblStatus.Text = "";
            RefreshClock();
        }

        private Pomodoro pomodoro;

    }
}
