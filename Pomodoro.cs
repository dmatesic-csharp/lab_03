﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PomodoroApp {
    internal class Pomodoro {

        public Pomodoro() : this(true, 25, 5) { 
        }

        public Pomodoro(bool WorkInProgress, int DurationWork, int DurationRest) {
            this.WorkInProgress = WorkInProgress;
            this.DurationWork = DurationWork;
            this.DurationRest = DurationRest; 
            CurrentSeconds = DurationWork;
        }

        public string Status() {
            if(WorkInProgress) {
                return "Rad u tijeku";
            } else {
                return "Odmor u tijeku";
            }
        }

        public void Duration() {
            CurrentSeconds--;
            if (CurrentSeconds < 0) {
                WorkInProgress = !WorkInProgress;
                if (WorkInProgress) {
                    CurrentSeconds = DurationWork;
                } else {
                    CurrentSeconds = DurationRest;
                }
            }
        }

        public override string ToString() {
            return String.Format("{0}:{1:00}", CurrentSeconds / 60, CurrentSeconds % 60);
        }

        public int CurrentSeconds { get; set; }
        public int DurationWork { get; set; }
        public int DurationRest { get; set; }
        public bool WorkInProgress{ get; set; }
    }
}
